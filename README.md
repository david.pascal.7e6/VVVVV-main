# P - VVVV
VVVVV Dark Wizard Dungeon
Descripción del Juego
VVVVV Dark Wizard Dungeon es un juego de rol de acción con una temática oscura y ambientado en un mundo de fantasía. En este juego, te sumergirás en el papel de un mago oscuro en un peligroso dungeon lleno de desafíos y enemigos. Explora una mazmorra misteriosa mientras luchas contra slimes y esqueletos. ¿Tienes lo que se necesita para sobrevivir en este mundo sombrío y lleno de magia negra?

Género
Género: Plataformas

Tematica: Fantasía oscura

Características Principales
Mago Oscuro: Juega como un mago oscuro.

Enemigos Aterradores: Enfréntate a desafiantes enemigos como slimes y esqueletos.

Exploración de Dungeon: Adéntrate en un dungeon lleno de secretos y peligros.

Requisitos del Sistema
Plataformas: PC.

Sistema Operativo: Windows 10 o superior.

Requisitos Mínimos:

Procesador: Intel Core i5
Memoria RAM: 4 GB
Tarjeta Gráfica: NVIDIA GeForce GTX 660
Almacenamiento: 2 GB de espacio disponible
Requisitos Recomendados:

Procesador: Intel Core i7
Memoria RAM: 8 GB
Tarjeta Gráfica: NVIDIA GeForce GTX 1060
Almacenamiento: 4 GB de espacio disponible
Capturas de Pantalla
Captura 1
Captura 2

Instrucciones de Juego
Control del Personaje: Utiliza las teclas de flecha para moverte hacia los lados y hacia arriba y la tecla v para canviar la gravedad.

Explora el Dungeon: Adéntrate en las ruinas del bosque y busca secretos.

Lucha contra Enemigos: Enfréntate a slimes y esqueletos con habilidades únicas.

Sobrevive: Que no te toqeun los enemigos.

Créditos
Desarrollador: David Pascal Baludo

Música y Sonido: Cojidas de internet.

Gráficos y Arte: Cojidos de itchi.io

Licencia
Este juego está protegido por derechos de autor. No está permitida la reproducción o distribución no autorizada.
