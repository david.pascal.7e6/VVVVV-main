using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RyTracingEnemigo : MonoBehaviour
{
    public Vector3 origin; // Coordenadas de origen del Raycast
    public float raycastDistance = 3f; // Distancia m�xima del Raycast
    public LayerMask obstacleLayer; // Capa de obst�culos que debe detectar el Raycast
    private bool movingRight = true; // Indica si el enemigo se mueve hacia la derecha
    public Animator _animator;

    private void Start()
    {
        // Inicializa el origen con la posici�n actual del GameObject
        origin = transform.position;
        _animator = gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        // Determina la direcci�n del Raycast
        Vector2 raycastDirection = movingRight ? Vector2.right : Vector2.left;

        // Realizar el Raycast
        RaycastHit2D hit = Physics2D.Raycast(origin, raycastDirection, raycastDistance, obstacleLayer);

        // Si el Raycast golpea un obst�culo o llega al l�mite de movimiento
        if (hit.collider != null || Mathf.Abs(transform.position.x - origin.x) >= 3f)
        {
            // Cambia la direcci�n de movimiento
            movingRight = !movingRight;
            // Voltea el sprite horizontalmente para que mire en la direcci�n correcta
            FlipSprite();
        }

        float moveSpeed = movingRight ? 1f : -1f;
        transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
    }

    private void FlipSprite()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
