using UnityEngine;
using UnityEngine.SceneManagement;

public class MovimientoPersonaje : MonoBehaviour
{
    public Vector2 respawnPosition;

    void Awake()
    {
        RespawnPlayer();
    }

    public void RespawnPlayer()
    {
        // Mueve al jugador a la posición de respawn (objeto vacío en la segunda escena).
        GameObject player = GameObject.Find("Player"); // Reemplaza con el nombre del objeto del jugador
        player.transform.position = respawnPosition;
    }
}
