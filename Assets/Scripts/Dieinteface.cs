using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dieinteface : MonoBehaviour
{
  
    public void RestartGame()
    {
        // Reinicia la escena especificada.
        SceneManager.LoadScene("Mapa1");
    }
    public void ExitGame()
    {
        // Sal del juego al modo de edici�n.
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
