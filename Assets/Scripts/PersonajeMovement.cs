using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeMovement : MonoBehaviour
{
    private Animator _animator;
    private Rigidbody2D _rb;
    private SpriteRenderer _sr;
    public AudioClip sonidoSalto; // Asigna el clip de audio en el Inspector
    private AudioSource jumpAudio;

    // Start is called before the first frame update
    void Start()
    {
        _animator = gameObject.GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
        _sr = GetComponent < SpriteRenderer>();
        jumpAudio = gameObject.AddComponent<AudioSource>(); // Agrega un componente AudioSource al objeto

        // Asigna el clip de audio al componente AudioSource
        jumpAudio.clip = sonidoSalto;
    }

    // Update is called once per frame
    void Update()
    {
        // Movimiento, etc...

        if (Input.GetKey(KeyCode.RightArrow))
        {
            _rb.velocity = new Vector2(7, _rb.velocity.y);
            _animator.SetBool("Mover", true);
            _sr.flipX = false;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _rb.velocity = new Vector2(-7, _rb.velocity.y);
            _animator.SetBool("Mover", true);
            _sr.flipX = true;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            _rb.velocity = Vector2.zero;
            _animator.SetBool("Mover", false);
        }
        if (Input.GetKeyDown(KeyCode.V) && _rb.velocity.y < 0.01f && _rb.velocity.y > -0.01f)
        {
            _rb.gravityScale *= -1;
            if (_rb.gravityScale == 1)
            {
                _sr.flipY = false;
            }
            else if (_rb.gravityScale == -1)
            {
                _sr.flipY = true;
            }
            jumpAudio.PlayOneShot(sonidoSalto); // Reproduce el sonido una vez
        }
    }
}
