using UnityEngine;

public class MocimientoBala : MonoBehaviour
{
    public float velocidad = 5.0f;
    private Vector3 startPosition;
    private Camera mainCamera;
    public AudioClip sonidoSalto; // Asigna el clip de audio en el Inspector
    private AudioSource jumpAudio;
    void Start()
    {
        startPosition = transform.position;
        mainCamera = Camera.main;
        jumpAudio = gameObject.AddComponent<AudioSource>(); // Agrega un componente AudioSource al objeto

        // Asigna el clip de audio al componente AudioSource
        jumpAudio.clip = sonidoSalto;
    }

    void Update()
    {
        transform.Translate(Vector3.left * velocidad * Time.deltaTime);

        if (!IsVisible())
        {
            Respawn();
        }
    }

    bool IsVisible()
    {
        // Comprueba si la bala est� dentro de la vista de la c�mara.
        Vector3 viewportPosition = mainCamera.WorldToViewportPoint(transform.position);
        return viewportPosition.x > 0 && viewportPosition.x < 1;
    }

    void Respawn()
    {
        // Reposiciona la bala en la posici�n inicial.
        transform.position = startPosition;
        jumpAudio.PlayOneShot(sonidoSalto); // Reproduce el sonido una vez
    }
}